from rest_framework.viewsets import ModelViewSet

from app_1.serializers import FirstSerializer

from app_1.models import FirstModel


class FirstView(ModelViewSet):
    serializer_class = FirstSerializer

    queryset = FirstModel.objects.all()
