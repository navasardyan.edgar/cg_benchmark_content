from rest_framework import routers
from main import views
from django.urls import include, path


router = routers.DefaultRouter()


router.register('projects', views.FirstView, basename='projects')

urlpatterns = [
    path('', include(router.urls)),
    path('first_url/', views.FirstView.as_view()),
]
