from rest_framework import serializers

from app_1.models import FirstModel


class FirstSerializer(serializers.ModelSerializer):

    class Meta:
        model = FirstModel
        fields = ('name',)
