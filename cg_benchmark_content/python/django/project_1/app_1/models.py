from django.db import models

from django.db.models import Model, CharField, ForeignKey


class FirstModel(Model):
    first_model_test_char_field = CharField(max_length=147, is_null=True, is_blank=True)

    class Meta:
        verbose_name = 'First model verbose name'
        verbose_name_plural = 'First model verbose name'


class SecondModel(Model):
    second_model_test_char_field = CharField(max_length=147, is_null=True, is_blank=True)
    second_model_test_foreign_field = ForeignKey(FirstModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Second model verbose name'
        verbose_name_plural = 'Second model verbose name'
